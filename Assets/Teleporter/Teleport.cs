﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;

public class Teleport : MonoBehaviour {
	
	public GameObject teledestination;
	public GameObject teledestination2;
	string fileName;
	public GameObject startCollider;

	// Use this for initialization
	void Start () {
		fileName = "subject.txt";
	}

	public void OnTriggerEnter(Collider o)
	{
		string timeStamp = o.GetComponent<Timer>().getTimeLeft();
		//File.AppendAllText(fileName, "End: "+timeStamp+"\n");
		startCollider.SetActive(true);
		int temp = o.GetComponent<Timer>().getRound()+1;
		o.GetComponent<Timer>().setRound(temp);
		Debug.Log("end time: "+ timeStamp);
		Debug.Log ("Teleport");
		if(o.GetComponent<Timer>().getRound() == 5)
		{
			Debug.Log("THE ROOM");
			o.transform.position = teledestination2.transform.position;
		}
		else{
			o.transform.position = teledestination.transform.position;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
