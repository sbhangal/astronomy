﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;


public class WaterBoxScript : MonoBehaviour {
	public GameObject water;
	private bool collected;
	private Vector3 startPos;
	public Transform target; 

	// Use this for initialization
	void Start () {
		water.GetComponent<Renderer>().enabled = false;
		startPos = water.GetComponent<Transform>().position;
		collected = false;
		Cursor.visible = true;

	}

	// Update is called once per frame
	void Update () {
		if (collected == true)
		{
			water.transform.Translate(0, 0.01f, 0);
			/*float step = 50*Time.deltaTime;
			water.transform.position = Vector3.Lerp(startPos, target.position, step);
			Debug.Log("target position "+target.position);*/
			//water.transform.position = Vector3.Lerp(startPos, target.position, 10*Time.deltaTime);
		}
	}

	public void OnTriggerEnter(Collider o) 
	{
		if (collected == false)
		{
			//The box is not rendered and the water appears
			GetComponent<Renderer>().enabled = false;
			collected = true;
			water.GetComponent<Renderer>().enabled = true;
			//water.transform.position = Vector3.Lerp(startPos, target.position, 10*Time.deltaTime);

		}
	}
}
