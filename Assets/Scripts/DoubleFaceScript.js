﻿#pragma strict
// This script was grabbed from:
// http://answers.unity3d.com/questions/280741/how-make-visible-the-back-face-of-a-mesh.html
// and 
// http://answers.unity3d.com/questions/442734/how-do-i-make-the-area-under-my-water-a-plane-look.html


function Start () {
    var mesh = GetComponent(MeshFilter).mesh;
    var vertices = mesh.vertices;
    var uv = mesh.uv;
    var normals = mesh.normals;
    var szV = vertices.length;
    var newVerts = new Vector3[szV*2];
    var newUv = new Vector2[szV*2];
    var newNorms = new Vector3[szV*2];
    for (var j=0; j< szV; j++){
        // duplicate vertices and uvs:
        newVerts[j] = newVerts[j+szV] = vertices[j];
        newUv[j] = newUv[j+szV] = uv[j];
        // copy the original normals...
        newNorms[j] = normals[j];
        // and revert the new ones
        newNorms[j+szV] = -normals[j];
    }
    var triangles = mesh.triangles;
    var szT = triangles.length;
    var newTris = new int[szT*2]; // double the triangles
    for (var i=0; i< szT; i+=3){
        // copy the original triangle
        newTris[i] = triangles[i];
        newTris[i+1] = triangles[i+1];
        newTris[i+2] = triangles[i+2];
        // save the new reversed triangle
        j = i+szT; 
        newTris[j] = triangles[i]+szV;
        newTris[j+2] = triangles[i+1]+szV;
        newTris[j+1] = triangles[i+2]+szV;
    }
    mesh.vertices = newVerts;
    mesh.uv = newUv;
    mesh.normals = newNorms;
    mesh.triangles = newTris; // assign triangles last!
}

/*var underwaterDens: float = 0.15;
 var underwaterColor: Color = Color(0.1, 0.3, 0.4, 1.0);
 
 private var oldFog: boolean;
 private var oldDens: float;
 private var oldColor: Color;
 private var oldMode: FogMode;
 private var curWater: Transform;
 private var underwater = false;


 function OnTriggerEnter(other: Collider){
     if (other.tag=="Water"){ // if entering a waterplane
         if (transform.position.y > other.transform.position.y){
             // set reference to the current waterplane
             curWater = other.transform; 
         }
     }
 }
 
 function OnTriggerExit(other: Collider){
     if (other.transform==curWater){ // if exiting the waterplane...
         if (transform.position.y > curWater.position.y){
             //  null the current waterplane reference
             curWater = null; 
         }
     }
 }
 
 function Update(){
     // if it's underwater...
     if (curWater && Camera.main.transform.position.y < curWater.position.y){
         if (!underwater){ // turn on underwater effect only once
             oldFog = RenderSettings.fog;
             oldMode = RenderSettings.fogMode;
             oldDens = RenderSettings.fogDensity;
             oldColor = RenderSettings.fogColor;
             RenderSettings.fog = true;
             RenderSettings.fogMode = FogMode.Exponential;
             RenderSettings.fogDensity = underwaterDens;
             RenderSettings.fogColor = underwaterColor;
             underwater = true;
         }
     } 
     else // but if it's not underwater...
     if (underwater){ // turn off underwater effect, if any
         RenderSettings.fog = oldFog;
         RenderSettings.fogMode = oldMode;
         RenderSettings.fogDensity = oldDens;
         RenderSettings.fogColor = oldColor;
         underwater = false;
     }
 }*/