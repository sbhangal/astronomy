﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.IO;
using UnityStandardAssets.Characters.FirstPerson;

//using System.Text.RegularExpressions;

public class Timer : MonoBehaviour {

	//public Text time;
	public int secondsLeft;
	int round; 
	public Canvas completion;
	public GameObject startCollider;
	public GameObject teledestination;
	public GameObject teledestination2;


	//public GameObject player;
	//public GameObject inputfield;
	//public InputField input;
	string fileName;

	//public GameObject TimeUp;

	// Use this for initialization
	void Start () {
		round = 1;
		completion.enabled = false;
		fileName = "subject.txt";
		StartCoroutine (Tick ());
		secondsLeft = 0;
	}

	public void startTime() {
		int minutes = secondsLeft / 60;
		int seconds = secondsLeft % 60;

		string UserTime = minutes.ToString("00") + ":" + seconds.ToString("00");
	}

	public void EndTime(string utime) {
		//File.AppendAllText(fileName, "End: "+utime+"\n");
		Debug.Log("end time: "+utime);
			
	}

	IEnumerator Tick() {
		// wait one second before changing state
		while(true)
		{
			yield return new WaitForSeconds (1);
			secondsLeft++;
		}

	}

	public void EndTimer() {
		//time.enabled = false;
		Debug.Log("Write out time.");
	}
	public int getRound() {
		return round;
	}
	public void setRound(int r){
		round = r;
	}
	// Update is called once per frame
	void Update () {
		if (round == 5)
		{
			if(Application.loadedLevelName == "mazeTraining")
			{
				Debug.Log("Finished Tasks");
				gameObject.GetComponent<FirstPersonController>().enabled = false;
				completion.enabled = true;
			}
			else{
				Debug.Log("THE ROOM??");
				gameObject.transform.position = teledestination2.transform.position;
			}
			if(Input.GetKeyDown("q"))
			{
				Debug.Log("Quit out of application");
				Application.Quit();
			}
		}
		Debug.Log("round = "+round);
		if (round <= 4)
		{
			if((Input.GetKeyDown("o") && Input.GetKey("p")) || (Input.GetKeyDown("p") && Input.GetKey("o")))
			{
				if(GetRawTime() - startCollider.GetComponent<StartTrigger>().GetStartTime() < 600)
				{
					//File.AppendAllText(fileName, "End: Gave up at time "+getTimeLeft()+"\n");
					Debug.Log("Gave up round "+round);
				}
				else 
				{
					//File.AppendAllText(fileName, "End: Time Limit Reached\n");
					Debug.Log("Time Limit Reached");
				}
				startCollider.SetActive(true);
				round++;
				gameObject.transform.position = teledestination.transform.position;
			}
		}

	}

	public string getTimeLeft() {
		int minutes = secondsLeft / 60;
		int seconds = secondsLeft % 60;

		string UserTime = minutes.ToString("00") + ":" + seconds.ToString("00");
		return UserTime;
	}

	public int GetRawTime() {
		return secondsLeft;
	}
}
