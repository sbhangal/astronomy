﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;

public class StartTrigger : MonoBehaviour {
	string fileName;
	public GameObject startCollider;
	private int startTime;
	// Use this for initialization
	void Start () {
		startTime = 0;
		fileName = "subject.txt";
		if (!File.Exists(fileName))
		{
			
			string testName = Application.loadedLevelName;
			Debug.Log(testName);
			//File.WriteAllText(fileName, testName+"\n");
		}
		else
		{
			string testName = Application.loadedLevelName;
			Debug.Log(testName);
			//File.AppendAllText(fileName, testName+"\n");
		}
	}
	public int GetStartTime() {
		return startTime;
	}

	public void OnTriggerEnter(Collider o) {
		startTime = o.GetComponent<Timer>().GetRawTime();
		string timeStamp = o.GetComponent<Timer>().getTimeLeft();
		//File.AppendAllText(fileName, "Round #"+o.GetComponent<Timer>().getRound()+"\nStart: "+timeStamp+"\n");
		startCollider.SetActive(false);
		Debug.Log("Round #"+o.GetComponent<Timer>().getRound());
		Debug.Log("start time: "+ timeStamp);
	}
	// Update is called once per frame
	void Update () {
	
	}
}
