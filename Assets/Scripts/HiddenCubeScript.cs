﻿using UnityEngine;
using System.Collections;

public class HiddenCubeScript : MonoBehaviour {
	private bool triggered;
	public GameObject[] fireworks;
	public GameObject[] flares;

	// Use this for initialization
	void Start () {
		triggered = false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnTriggerEnter(Collider o) 
	{
		if (triggered == false)
		{
			//The box is not rendered and the water appears
			//GetComponent<Renderer>().enabled = false;
			triggered = true;
			foreach (GameObject i in fireworks) {
				i.GetComponent<GlowParticlesScript>().MakeVisible();
			}
			//yield return new WaitForSeconds(2);
			foreach (GameObject j in flares) {
				j.GetComponent<GlowParticlesScript>().MakeVisible();
			}
			//water.GetComponent<Renderer>().enabled = true;
			//water.transform.position = Vector3.Lerp(startPos, target.position, 10*Time.deltaTime);

		}
	}
}
