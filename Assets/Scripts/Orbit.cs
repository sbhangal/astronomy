﻿using UnityEngine;
using System.Collections;

public class Orbit : MonoBehaviour {

	//sun and planet
	public GameObject sun, planet;

	//sun's X axis
	public float sunX;
	//sun's Y axis
	public float sunZ;

	//planet's X axis
	public float planetX;
	public float planetZ;

	//major axis
	public float majorAxis;
	//minor axis
	public float minorAxis;

	//speed of Orbit
	public float alpha;

	public float newPlanetX;
	public float newPlanetY; 


	float xDiff, yDiff, zDiff;
	float minDist;
	int itemIndexToDisplay, length;
	int i;
	int totalObjects;
	int popUpDistance;

	public GameObject user; //The object for the user position


	// Use this for initialization
	void Start () {
		
		planetX = planet.transform.position.x;
		planetZ = planet.transform.position.z;
		//sunX = GameObject.Find("Sun").transform.position.x;
		//sunY = GameObject.Find("Sun").transform.position.y;
	
	}

	// Update is called once per frame
	void Update () {
		
		//check x position
		xDiff = Mathf.Abs(user.transform.position.x - planet.transform.position.x);
		if (xDiff < 20) 
		{
			//check y position
			yDiff = Mathf.Abs(user.transform.position.y - planet.transform.position.y);
			if (yDiff < 20) 
			{
				//check z position
				zDiff = Mathf.Abs (user.transform.position.z - planet.transform.position.z);
				if (zDiff < 20) 
				{
				}
				else
				{
					majorAxis = 30f;
					minorAxis = 15;
					sunX = sun.transform.position.x;
					sunZ = sun.transform.position.z;

					planet.transform.position = new Vector3 (sunX + (majorAxis * Mathf.Sin (Mathf.Deg2Rad * alpha)), 0, sunZ + (minorAxis * Mathf.Cos (Mathf.Deg2Rad * alpha)));
					//newPlanetX = alpha;
					//newPlanetY = Mathf.Sin(alpha);
					//newPlanetX = sunX + (majorAxis * Mathf.Cos(alpha * 0.005f));
					//newPlanetY = sunY + (minorAxis * Mathf.Sin(alpha * 0.005f));
					alpha += 1f;
					//planet.gameObject.transform.position = sun.transform.position + new Vector3(newPlanetX,0,newPlanetY);
					print(planet.gameObject.transform.position.x);
				}
			}
			else
			{
				majorAxis = 30f;
				minorAxis = 15;
				sunX = sun.transform.position.x;
				sunZ = sun.transform.position.z;

				planet.transform.position = new Vector3 (sunX + (majorAxis * Mathf.Sin (Mathf.Deg2Rad * alpha)), 0, sunZ + (minorAxis * Mathf.Cos (Mathf.Deg2Rad * alpha)));
				//newPlanetX = alpha;
				//newPlanetY = Mathf.Sin(alpha);
				//newPlanetX = sunX + (majorAxis * Mathf.Cos(alpha * 0.005f));
				//newPlanetY = sunY + (minorAxis * Mathf.Sin(alpha * 0.005f));
				alpha += 1f;
				//planet.gameObject.transform.position = sun.transform.position + new Vector3(newPlanetX,0,newPlanetY);
				print(planet.gameObject.transform.position.x);
			}
		}

		//if the user is not close enough, the sign does not display
		else
		{
			majorAxis = 30f;
			minorAxis = 15;
			sunX = sun.transform.position.x;
			sunZ = sun.transform.position.z;

			planet.transform.position = new Vector3 (sunX + (majorAxis * Mathf.Sin (Mathf.Deg2Rad * alpha)), 0, sunZ + (minorAxis * Mathf.Cos (Mathf.Deg2Rad * alpha)));
			//newPlanetX = alpha;
			//newPlanetY = Mathf.Sin(alpha);
			//newPlanetX = sunX + (majorAxis * Mathf.Cos(alpha * 0.005f));
			//newPlanetY = sunY + (minorAxis * Mathf.Sin(alpha * 0.005f));
			alpha += 1f;
			//planet.gameObject.transform.position = sun.transform.position + new Vector3(newPlanetX,0,newPlanetY);
			print(planet.gameObject.transform.position.x);
		}
	}
}
