﻿using UnityEngine;
using System.Collections;

public class GlowParticlesScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		GetComponent<ParticleSystem>().Pause();

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void MakeVisible() {
		GetComponent<ParticleSystem>().Play();
	}
}
