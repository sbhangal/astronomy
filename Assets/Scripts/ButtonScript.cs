﻿using UnityEngine;
using System.Collections;

public class ButtonScript : MonoBehaviour {
	public GameObject chair;
	public GameObject floor;
	public GameObject floor2;
	private bool chairButton;
	private bool planeButton;
	private int count;

	// Use this for initialization
	void Start () {
		Cursor.visible = true;
		chairButton = false;
		planeButton = false; 
		count = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if (chairButton == true) {
			chair.transform.Rotate(Vector3.back, 5000f * Time.deltaTime);
			if (count > 50) {
				chair.transform.Translate(0, 0, 1);
			}
			count++;
		}
		if (planeButton == true) {
			floor.GetComponent<MeshCollider>().enabled = false;
			floor2.GetComponent<MeshCollider>().enabled = false;
		}
	}

	/*void OnMouseOver() {
		if (Input.GetMouseButtonDown(0)) {
			if(gameObject.name == "Small button") {
				transform.Translate(1, 0, 0);
				chairButton = true;
				//chair.transform.Rotate(Vector3.up, 10f * Time.deltaTime);
			}
			else if(gameObject.name == "GiantButton") {
				transform.Translate(0, -1, 0);
				planeButton = true;
			}
		}
	}*/

	void OnTriggerEnter(Collider o){
		if(gameObject.name == "Small button") {
			GetComponent<MeshRenderer>().enabled = false;
			chairButton = true;
		}
		else if(gameObject.name == "GiantButton") {
			GetComponent<MeshRenderer>().enabled = false;
			planeButton = true;
		}
	}

}
