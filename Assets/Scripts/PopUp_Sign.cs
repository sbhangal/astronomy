﻿/*
 * File: PopUp_Sign.cs
 * Programmer: Shandip Bhangal
 * Objective: To enable the sign to display next to a planet if the user gets close enough
 * 
 * */

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PopUp_Sign : MonoBehaviour {
	public GameObject pc, ee, w, sun; //The four stars: Proxima Centauri, Epsilon Eridani, Wolf 1061, and the Sun  
	public GameObject pcb, eeb, wc, earth; //The four planets: Proxima Centauri b, Epsilon Eridani b, Wolf 1061c, and the Earth
	public GameObject user; //The object for the user position
	GameObject sign; //Sign to be displayed



	float xDiff, yDiff, zDiff;
	float minDist;
	int itemIndexToDisplay, length;
	int i;
	int totalObjects;
	public GameObject[] celestialObjects;
	int popUpDistance;

	// Use this for initialization
	void Start () {
		totalObjects = 8; //total objects (stars + planets)

		celestialObjects = new GameObject[totalObjects]; //stars and planets
		length = celestialObjects.Length;

		celestialObjects [0] = pc;
		celestialObjects [1] = ee;
		celestialObjects [2] = w;
		celestialObjects [3] = sun;
		celestialObjects [4] = pcb;
		celestialObjects [5] = eeb;
		celestialObjects [6] = wc;
		celestialObjects [7] = earth;

		for (i = 0; i < length; i++) 
		{
			//sign = GameObject.Find (celestialObjects [i].name + "Sign");
			celestialObjects [i].GetComponentInChildren<SpriteRenderer> ().enabled = false;
		}

		popUpDistance = 20;
	}
	
	// Update is called once per frame
	void Update () {

		for (i = 0; i < length; i++) 
		{
			//check x position
			xDiff = Mathf.Abs(user.transform.position.x - celestialObjects[i].transform.position.x);
			if (xDiff < 20) 
			{
				//check y position
				yDiff = Mathf.Abs(user.transform.position.y - celestialObjects[i].transform.position.y);
				if (yDiff < 20) 
				{
					//check z position
					zDiff = Mathf.Abs (user.transform.position.z - celestialObjects [i].transform.position.z);
					if (zDiff < 20) 
					{
						celestialObjects [i].GetComponentInChildren<SpriteRenderer> ().enabled = true;
						//sign = GameObject.Find (celestialObjects [i].name + "Sign");
						//sign.GetComponent<SpriteRenderer> ().enabled = true;

					}
					else
					{
						celestialObjects [i].GetComponentInChildren<SpriteRenderer> ().enabled = false;
						//sign = GameObject.Find(celestialObjects[i].name + "Sign");
						//sign.GetComponent<SpriteRenderer> ().enabled = false;
					}
				}
				else
				{
					celestialObjects [i].GetComponentInChildren<SpriteRenderer> ().enabled = false;
					//sign = GameObject.Find(celestialObjects[i].name + "Sign");
					//sign.GetComponent<SpriteRenderer> ().enabled = false;
				}
			}

			//if the user is not close enough, the sign does not display
			else
			{
				celestialObjects [i].GetComponentInChildren<SpriteRenderer> ().enabled = false;
				//sign = GameObject.Find(celestialObjects[i].name + "Sign");
				//sign.GetComponent<SpriteRenderer> ().enabled = false;
			}
		}

	}


}
