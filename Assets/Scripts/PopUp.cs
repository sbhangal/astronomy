﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PopUp : MonoBehaviour {

	public GameObject block;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Change()
	{
		//Toggles the pop up screen or item
		if(this.gameObject.activeInHierarchy == true)
		{
			this.gameObject.SetActive(false);
			block.gameObject.SetActive (false);
		}
		else
		{
			this.gameObject.SetActive(true);
			block.gameObject.SetActive (true);
		}
	}
}
