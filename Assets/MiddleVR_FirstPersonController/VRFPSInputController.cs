/* VRFPSInputController
 * (c) MiddleVR
 */

using UnityEngine;
using System.Collections;
using MiddleVR_Unity3D;
using System;
using UnityStandardAssets.Characters.FirstPerson;

[RequireComponent(typeof(CharacterMotor))]
[RequireComponent(typeof(MouseLook))]
[RequireComponent(typeof(FPSInputController))]
public class VRFPSInputController : MonoBehaviour
{
    public string ReferenceNode    = "HandNode";
    public bool   Strafe           = false;
    public bool   m_EnableJump     = false;
    public int    m_JumpWandButton = 1;

    private bool m_HasLoggedReferenceNodeWarning = false;
    private GameObject m_ReferenceNodeObject     = null;
    
    // Use this for initialization
    protected void Start()
    {
        GameObject VRManagerObject = GameObject.Find("VRManager");
        if (VRManagerObject != null)
        {
            // Disable MouseLook and FPSInputController scripts
            GetComponent<MouseLook>().enabled = false;
            GetComponent<FPSInputController>().enabled = false;

            if(VRManagerObject.GetComponent<VRManagerScript>().VRSystemCenterNode != gameObject)
            {
                MVRTools.Log("[ ] VRFPSInputController: Make sure you make the 'VRSystem Center Node' property of the 'VRManager' object point to the 'First Person Controller' object.");
            }
        }
        else
        {
            Debug.Log("[X] VRManager object not found, disabling VRFPSInputController!");
            enabled = false;
        }
    }

    protected void Update()
    {
        vrInteraction activeNavigationInteraction = MiddleVR.VRInteractionMgr.GetActiveInteractionByTag("ContinuousNavigation");
        if (activeNavigationInteraction != null)
        {
            MiddleVR.VRInteractionMgr.Deactivate(activeNavigationInteraction);
            MVRTools.Log("[ ] VRFPSInputController: Make sure you disable the default Navigation Interaction by setting the 'Navigation' property of the 'VRManager' object to None.");
        }

        CharacterMotor characterMotor = GetComponent<CharacterMotor>();
        vrKeyboard keyboard = MiddleVR.VRDeviceMgr.GetKeyboard();

        if (m_ReferenceNodeObject == null)
        {
            m_ReferenceNodeObject = GameObject.Find(ReferenceNode);
        }

        float speed = 0.0f;
        float speedR = 0.0f;

        // Choosing active vertical axis
        float forward = 0.0f;

        // 1. Test Unity wand input
        float verticalAxis = MiddleVR.VRDeviceMgr.GetWandVerticalAxisValue();
        if (Mathf.Abs(verticalAxis) > 0.1f)
        {
            forward = verticalAxis;
        }
        else
        {
            // 2. Test MiddleVR keyboard input
            if (keyboard != null && keyboard.IsKeyPressed(MiddleVR.VRK_DOWN))
            {
                forward = -1.0f;
            }
            else if (keyboard != null && keyboard.IsKeyPressed(MiddleVR.VRK_UP))
            {
                forward = 1.0f;
            }
            else
            {
                // 3. Test Unity input
                verticalAxis = Input.GetAxis("Vertical");
                if (Mathf.Abs(verticalAxis) > Mathf.Epsilon)
                {
                    forward = verticalAxis;
                }
            }
        }

        // Computing speed
        if (Math.Abs(forward) > 0.1)
        {
            speed = forward * Time.deltaTime * 30;
        }

        // Choosing active horizontal axis
        float rotation = 0.0f;

        // 1. Test Unity wand input
        float horizontalAxis = MiddleVR.VRDeviceMgr.GetWandHorizontalAxisValue();
        if (Mathf.Abs(horizontalAxis) > 0.1f)
        {
            rotation = horizontalAxis;
        }
        else
        {
            // 2. Test MiddleVR keyboard input
            if (keyboard != null && keyboard.IsKeyPressed(MiddleVR.VRK_LEFT))
            {
                rotation = -1.0f;
            }
            else if (keyboard != null && keyboard.IsKeyPressed(MiddleVR.VRK_RIGHT))
            {
                rotation = 1.0f;
            }
            else
            {
                // 3. Test Unity input
                horizontalAxis = Input.GetAxis("Horizontal");
                if (Mathf.Abs(horizontalAxis) > Mathf.Epsilon)
                {
                    rotation = horizontalAxis;
                }
            }
        }

        // Compute rotation speed
        if (Math.Abs(rotation) > 0.1)
        {
            speedR = rotation * Time.deltaTime * 50;
        }

        Vector3 directionVector = new Vector3(0, 0, speed);

        if (Strafe)
        {
            directionVector.x = speedR;
        }
        else
        {
            transform.Rotate(Vector3.up, speedR);
        }

        if (m_ReferenceNodeObject == null)
        {
            if (!m_HasLoggedReferenceNodeWarning)
            {
                MVRTools.Log("[ ] VRFPSInputController: Didn't find reference node '" + ReferenceNode + "'.");
                m_HasLoggedReferenceNodeWarning = true;
            }

            characterMotor.inputMoveDirection = directionVector;
        }
        else
        {
            characterMotor.inputMoveDirection = m_ReferenceNodeObject.transform.TransformDirection(directionVector);
        }

        // Handle jump
        if (m_EnableJump)
        {
            if (Input.GetButton("Jump")
                || MiddleVR.VRDeviceMgr.IsWandButtonToggled((uint)m_JumpWandButton)
                || (keyboard != null && keyboard.IsKeyPressed(MiddleVR.VRK_SPACE)))
            {
                characterMotor.inputJump = true;
            }
            else
            {
                characterMotor.inputJump = false;
            }
        }
    }
}
